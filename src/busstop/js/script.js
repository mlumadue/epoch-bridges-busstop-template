var playing = 0;
var isTouch = 0;

jQuery(function($) { 

	$(document).ready(function() {

		// updated fix for placeholders in IE 8 & 9
		// from https://gist.github.com/pjeweb/1320822
		// fork of https://gist.github.com/hagenburger/379601
		
		$('[placeholder]')
	        .focus(function () {
	            var input = $(this);
	            if (input.val() === input.attr('placeholder')) {
	                input.val('').removeClass('placeholder');
	            }
	        })
	        .blur(function () {
	            var input = $(this);
	            if (input.val() === '' || input.val() === input.attr('placeholder')) {
	                input.addClass('placeholder').val(input.attr('placeholder'));
	            }
	        })
	        .blur()
	        .parents('form').submit(function () {
	            $(this).find('[placeholder]').each(function () {
	                var input = $(this);
	                if (input.val() === input.attr('placeholder')) {
	                    input.val('');
	                }
	            });
	        });		
		
		// end fix for form placeholders in IE 8 & 9



		// try our best to detect a touch screen
		// if there is a touch screen, layout the page with the form away from the video
		
		if (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)) {
	
			touchnplay();
			isTouch = 1;
		
		}
		
		// end check for touch
		
		// fire up the video
		
		// var video = '<iframe class="video" id="player" src="https://www.youtube.com/embed/GUoi9xKcdmo?enablejsapi=1&origin=http://bridgesbyepoch.com" frameborder="0" allowfullscreen></iframe>';
				
		$('body.front #hero #iframe-container').on('click touch', function() {
			
			if (playing === 0) {
				// call the function to move everything around so the video is not blocked by elements
				touchnplay();
				$('#hero').removeClass('hero-back').addClass('hero-noback');
				$('.video').css('display', 'block');
				player.playVideo();
				playing = 1;		
			}
			
			
		});
	}); // end document ready

	function touchnplay() {
		$('#right').removeClass('right-novid').addClass('right-playvid');
		// $('#right').css('top', '0px').css('display', 'block');
		$('#webform-tablet').css('display', 'none');
		$('#content').removeClass('col-md-12').addClass('col-md-8');
		$('#secondary-images').css('margin-top', '25px');		
	}
	
	
	
}); // end jQuery

// after the video is done playing, put everything back if not touch screen

function endPlay() {
	if (isTouch === 0) {
		jQuery('#right').removeClass('right-playvid').addClass('right-novid');
		// jQuery('#right').removeAttr('style');
		jQuery('#webform-tablet').removeAttr('style');
		jQuery('#content').removeClass('col-md-8').addClass('col-md-12');
		jQuery('#secondary-images').removeAttr('style');
	}
	jQuery('.video').css('display', 'none');
	jQuery('#hero').removeClass('hero-noback').addClass('hero-back');
	// set the playing state to 'no' - happens even if the screen is touch
	playing = 0;
}