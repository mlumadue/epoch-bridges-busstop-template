	<div id="page-wrapper">
		<header id="header-wrapper" class="section clearfix">
			<div id="header">
				<section class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<?php if ($logo): ?>
						<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
							<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>">
						</a>
					<?php endif; ?>
				</section>
				<section class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<?php print render($page['header']); ?>
				</section>
			</div>
		</header> <!-- /.section, /#header -->

		<?php if ($page['nav']): ?>
			<nav id="navigation" class="clearfix">
				<?php print render($page['nav']); ?>
			</nav> <!-- /.section, /#navigation -->
		<?php endif; ?>
    
		<?php if ($breadcrumb): ?>
			<div id="breadcrumb" class="clearfix"><?php print $breadcrumb; ?></div>
		<?php endif; ?>
		<?php if ($messages): ?>
			<div id="messages" class="clearfix"><?php print $messages; ?></div>
		<?php endif; ?>
		
		<div id="hero-wrapper">
			<div id="hero" class="hero-back">
				<?php if ($page['hero']): ?>
					<?php print render($page['hero']); ?>
				<?php endif;?>
			</div>
		</div>	

		<div id="main-wrapper" class="clearfix">
			<div id="main" class="clearfix">
			
				<?php if ($page['highlighted']): ?>
					<section id="highlighted" class="clearfix">
						<?php print render($page['highlighted']); ?>
					</section>
				<?php endif; ?>
			
			
				<section id="content" class="col-xs-12 <?php if ($page['right']):print 'col-sm-12 col-md-12 col-push-md-4 col-lg-8 col-push-lg-4';else:print 'col-sm-12 col-md-12 col-lg-12 clearfix';endif; ?>">
					<a id="main-content"></a>
					<header>
						<?php print render($title_prefix); ?>
						<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
						<?php print render($title_suffix); ?>
						<?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
						<?php print render($page['help']); ?>
						<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
					</header>
					<?php print render($page['content']); ?>
					<footer>
					  <?php print $feed_icons; ?>
					</footer>
				</section> <!-- /.section, /#content -->
				<?php if ($page['right']): ?>
					<aside id="right" class="col-xs-12 col-sm-12 col-md-4 col-lg-4 right-novid sidebar">
						<?php print render($page['right']); ?>
					</aside> <!-- /#right -->
				<?php endif; ?>
			</div><!-- /#main -->
			<?php if ($page['secondary']): ?>
				<aside id="secondary" class="clearfix">
					<?php print render($page['secondary']); ?>
				</aside> <!-- /.section, /#secondary -->
			<?php endif; ?>
			<?php if ($page['tertiary']): ?>
				<aside id="tertiary" class="clearfix">
					<?php print render($page['tertiary']); ?>
				</aside> <!-- /.section, /#secondary -->
			<?php endif; ?>
		</div> <!-- /#main-wrapper -->
		<?php if ($page['bottom-phone']): ?>
			<div id="bottom-phone">
				<?php print render($page['bottom-phone']); ?>
			</div>
		<?php endif ?>

		<footer id="footer-wrapper" class="clearfix">
			<div id="footer">
				<div class="section">
					<?php print render($page['footer']); ?>
				</div>
				<div class="section">
					<?php print render($page['copyright']); ?>
				</div>
			</div>
		</footer> <!-- /.section, /#footer -->
	</div> <!-- /#page, /#page-wrapper -->
