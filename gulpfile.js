var gulp = require('gulp');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var autoprefix = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var runSequence = require('run-sequence');
var sftp = require("gulp-sftp");

var localSrc = './src/busstop';
var localDest = './build/busstop';
var host = 'www.bridgesbyepoch.com';
var user = 'bridgesbyepoch';
var pass = 'Ep0chBr1dg3Z';
var ftpDest = '/docroot/busstop/sites/all/themes/busstop';


gulp.task('jshint', function() {
  gulp.src(localSrc + '/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('uglify', function() {
  gulp.src(localSrc + '/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest(localDest + '/js'))
	.pipe(sftp({
		host: host,
        user: user,
        pass: pass,
		remotePath: ftpDest + '/js'
	}));
});

gulp.task('style', function(callback) {
	runSequence('sass', 'css', callback);
});

gulp.task('sass', function(){
	return gulp.src(localSrc + '/sass/*.scss')
		.pipe(sass())
		.pipe(gulp.dest(localSrc + '/css'));
		
});

gulp.task('css', function() {
  gulp.src([localSrc + '/css/*.css'])
    .pipe(concat('style.css'))
    .pipe(autoprefix('last 2 versions', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest(localDest + '/css/'))
	.pipe(sftp({
		host: host,
        user: user,
        pass: pass,
		remotePath: ftpDest + '/css'
	}));
});

gulp.task('templates', function() {
	gulp.src([localSrc + '/templates/*'])
		.pipe(gulp.dest(localDest + '/templates'))
		.pipe(sftp({
			host: host,
			user: user,
			pass: pass,
			remotePath: ftpDest + '/templates'
		}));
});

gulp.task('root', function() {
	gulp.src([localSrc + '/*'])
		.pipe(gulp.dest(localDest))
		.pipe(sftp({
			host: host,
			user: user,
			pass: pass,
			remotePath: ftpDest
		}));
});

gulp.task('default', ['uglify', 'style', 'templates', 'root'], function() {
	
	gulp.watch(localSrc + '/js/*.js', function() {
		gulp.run('jshint', 'uglify');
		// gulp.run('uglify');
	});
	gulp.watch(localSrc + '/sass/*.scss', function() {
		gulp.run('style');
	});
	gulp.watch(localSrc + '/templates/*.php', function() {
		gulp.run('templates');
	});
	gulp.watch([localSrc + '/*.info', localSrc + '/*.php'], function() {
		gulp.run('root');
	});
});